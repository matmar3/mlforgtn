#!/bin/bash

# sets home directory
DATADIR="/storage/plzen1/home/$LOGNAME/sar_sp/mlforgtn/"

# enters user's scratch directory
cd $DATADIR || exit 1

module add python36-modules-gcc

# Set pip path for --user option
export PYTHONUSERBASE=/storage/plzen1/home/martinm/.local

# set PATH and PYTHONPATH variables
export PATH=$PYTHONUSERBASE/bin:$PATH
export PYTHONPATH=$PYTHONUSERBASE/lib/python3.6/site-packages:$PYTHONPATH

# loads modules
pip install -r ./requirements.txt --user

# setup SCRATCH cleaning in case of an error
trap 'clean_scratch' TERM EXIT

# start ner
python ./run_analysis.py
