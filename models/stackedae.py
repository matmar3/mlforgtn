import keras
import sys
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Dense, Activation, Dropout, Input


def get_stackedae(input_dimension):
    # this is the size of our encoded representations
    encoding_dim = 100  # 32 floats -> compression of factor 24.5, assuming the input is 784 floats
    
    # this is our input placeholder
    input_layer = Input(shape=(input_dimension,))
    # "encoded" is the encoded representation of the input
    encoded = Dense(encoding_dim, activation='relu')(input_layer)
    # "decoded" is the lossy reconstruction of the input
    decoded = Dense(input_dimension, activation='sigmoid')(encoded)

    # encoder module
    # this model maps an input to its reconstruction
    autoencoder = Model(input_layer, decoded)
    # this model maps an input to its encoded representation
    encoder = Model(input_layer, encoded)
    
    # devoder module
    # create a placeholder for an encoded (32-dimensional) input
    encoded_input = Input(shape=(encoding_dim,))
    # retrieve the last layer of the autoencoder model
    decoder_layer = autoencoder.layers[-1]
    # create the decoder model
    decoder = Model(encoded_input, decoder_layer(encoded_input))
    
    autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')