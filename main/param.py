from sklearn import svm
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn import neighbors
import numpy as np


class Param:

    def __init__(self):

        # file containing all extracted Guess The Number epochs
        # Data format:
        # number of epochs x number of EEG channels x  number of time samples in each epoch
        self.input_file = './VarekaGTNEpochs.mat'

        # input / outputs configuration
        self.show_plots = False
        self.verbose = False

        # classification settings
        self.n_epochs = 30
        self.validation = 0.25
        self.val_iter = 30
        self.loss = 'categorical_crossentropy'
        # self.loss = 'binary_crossentropy'
        self.svm = svm.SVC(C=1.0, cache_size=500, class_weight=None, coef0=0.0,
                           decision_function_shape='ovr', degree=3, gamma='scale', kernel='rbf',
                           probability=False, shrinking=True,
                           verbose=False)
        self.lda = LinearDiscriminantAnalysis(solver='eigen', shrinkage='auto')
        self.knn = neighbors.KNeighborsClassifier(15, weights='distance')

        # feature extraction settings
        self.pre_epoch = -0.2
        self.sampling_fq = 1000
        self.rej_threshold = 100
        self.training_avg = 1
        self.testing_avg = 1
        min_latency = 0.3
        max_latency = 1
        steps = 21
        temp_wnd = np.linspace(min_latency, max_latency, steps)
        self.intervals = np.zeros((steps - 1, 2))
        for i in range(0, temp_wnd.shape[0] - 1):
            self.intervals[i, 0] = temp_wnd[i]
            self.intervals[i, 1] = temp_wnd[i + 1]
        self.intervals = self.intervals - self.pre_epoch

        # plotting
        self.plot_channel = 2