import os.path
from pylab import *
from main.param import Param
import models.cnn
import numpy as np
from sklearn.model_selection import LeavePOut
from sklearn.model_selection import ShuffleSplit
import scipy.io as sio
import models.sklearnclassifier
import main.pre_processing
import output.plots
from keras.utils import plot_model
from sklearn.model_selection import train_test_split

# for reproducibility
from numpy.random import seed
from tensorflow import set_random_seed
seed(1)
set_random_seed(2)

# for training on CPU only
# import os
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

# load user defined parameters
param = Param()

if not os.path.exists(param.input_file):
    print("Input file not exists!")
    exit(-1)

# input data from Matlab
data = sio.loadmat(param.input_file)
target = data['allTargetData']
non_target = data['allNonTargetData']

# prepare arrays containing training and testing data and labels
out_features = np.concatenate((target, non_target), axis=0)
out_t_labels = np.tile(np.array([1, 0]), (target.shape[0], 1))
out_n_labels = np.tile(np.array([0, 1]), (non_target.shape[0], 1))
out_labels = np.vstack((out_t_labels, out_n_labels))

# pre-processing and feature extraction
out_features, out_labels = main.pre_processing.reject_amplitude(out_features, out_labels, param)

# plot averages just for control
if param.show_plots:
    output.plots.plot_average(out_features, out_labels, param)

# select one of the following pre-processing methods
# out_features = main.pre_processing.windowed_means(out_features, param)
out_features = main.pre_processing.cnn_reshape(out_features)

# hold one part out for testing at the end of the process
x_train, x_test, y_train, y_test = train_test_split(out_features, out_labels, test_size=param.validation,
                                                    random_state=0, shuffle=True)
val = round(param.validation * x_train.shape[0])

# cross-validation
leave_p_out = LeavePOut(p=val)
shuffle_split = ShuffleSplit(n_splits=param.val_iter, test_size=val, random_state=0)

# averages training epochs if requested
[x_train, y_train] = main.pre_processing.neighbor_average_all(x_train, y_train, param.training_avg)

# average epochs in the testing set if requested
[x_test, y_test] = main.pre_processing.neighbor_average_all(x_test, y_test, param.testing_avg)

counter = 0
val_results = []
test_results = []

# perform training (+ inner validation) and testing for
# all cross-validation pairs
for train, validation in shuffle_split.split(x_train):
    counter = counter + 1

    # select one of the following models
    model = models.cnn.CNN(x_train.shape[1], x_train.shape[2], param)
    # model = models.sklearnclassifier.SkLearnClassifier(param.lda, param)

    validation_metrics = model.fit(x_train[train], y_train[train], x_train[validation], y_train[validation])
    val_results.append(validation_metrics)

    # evaluate the model using the fixed testing set as the last step of processing
    test_metrics = model.evaluate(x_test, y_test)
    test_results.append(test_metrics)

# print averages and standard deviations of results of testing and validation
print("Validation results: ", val_results, ", \naverage: ", np.round(np.mean(val_results, axis=0) * 100, 2), "\nstd: ",
      np.round(np.std(val_results, axis=0) * 100, 2))
print("Test results: ", test_results, ", \naverage: ", np.round(np.mean(test_results, axis=0) * 100, 2), "\nstd: ",
      np.round(np.std(test_results, axis=0) * 100, 2))
print("Test data size: ", np.shape(y_test))

if param.show_plots:
    #   output.plots.display_history(hist)
    plot_model(model.model, to_file='output/model.svg', show_shapes=True, show_layer_names=True,  rankdir='LR')




