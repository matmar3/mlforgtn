#!/bin/bash

# loads modules
pip3 install -r ./local_requirements.txt --user

# start ner
python3 ./run_analysis.py
