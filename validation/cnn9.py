import keras
import sys
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Conv2D, AveragePooling2D, Dense, Flatten, Activation, Dropout, Input, MaxPooling2D
from keras.layers.normalization import BatchNormalization
import keras_metrics
from keras.callbacks import EarlyStopping
import output.plots as display
import tensorflow as tf
from keras import backend as K


def auc(y_true, y_pred):
    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc


# Applies convolutional neural network
# to both training and testing data
class CNN:

    # Configuration of the model
    def __init__(self, channels, time_samples, param):
        self.model = Sequential((
            # The first conv layer learns `nb_filter` filters (aka kernels),
            # each of size ``(filter_length, nb_input_series)``.
            # Its output will have shape (None, window_size - filter_length + 1, nb_filter), i.e., for each position in
            # the input timeseries, the activation of each filter at that position.
            Conv2D(6, (3, 3), activation='elu', input_shape=(channels, time_samples, 1)),
            BatchNormalization(),
            Dropout(0.2),  # added 28. 05. 2019

            AveragePooling2D(pool_size=(1, 8)),  # Downsample the output of convolution by 2X.
            Flatten(),

            Dense(100, activation='elu'),
            BatchNormalization(),
            Dropout(0.2),

            Dense(2, activation='softmax'),
        ))
        # To perform (binary) classification instead:
        self.model.compile(loss=param.loss, optimizer='adam',
                           metrics=[auc, 'binary_accuracy', keras_metrics.precision(), keras_metrics.recall()])
        self.param = param
        if self.param.verbose:
            print(self.model.summary())

    def get_model(self):
        return self.model

    # Fits the model using training data and applies early stopping based
    # on the validation subset:
    # x[number of examples x number of channels x samples in each epoch x 1]
    # y[number of examples x number of categories - default 2]
    # self.param.validation determines the validation set size
    def fit(self, x_train, y_train, x_val, y_val):
        early_stopping = EarlyStopping(monitor='val_loss', patience=5, verbose=1, mode='auto')
        hist = self.model.fit(x_train, y_train, epochs=self.param.n_epochs, batch_size=16, shuffle=True,
                              callbacks=[early_stopping], verbose=self.param.verbose,
                              validation_data=(x_val, y_val))
        val_metrics = [hist.history['val_auc'][-1], hist.history['val_binary_accuracy'][-1],
                       hist.history['val_precision'][-1], hist.history['val_recall'][-1]]
#        if self.param.show_plots:
#            display.display_history(hist)
        return val_metrics  # , hist

    def evaluate(self, x, y):
        metrics = self.model.evaluate(x, y)
        if self.param.verbose:
            print(self.model.metrics_names)
        return metrics
